package pubsub

import (
	"context"
	"sync/atomic"
	"testing"
	"time"

	"gitlab.com/kd-org/golib/native"
)

func TestTopic(t *testing.T) {
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	var topic Topic[string]
	sub1, err := topic.Subscribe(10)
	if err != nil {
		t.Fatal(err)
	}
	defer sub1.Close()
	sub2, err := topic.Subscribe(10)
	if err != nil {
		t.Fatal(err)
	}
	defer sub2.Close()

	var sub1Count int32
	var sub2Count int32
	go func() {
		for {
			select {
			case <-ctx.Done():
				return
			case <-sub1.Events():
				atomic.AddInt32(&sub1Count, 1)
			case <-sub2.Events():
				atomic.AddInt32(&sub2Count, 1)
			}
		}
	}()

	for i := 0; i < 5; i++ {
		if err := topic.Publish(native.Pointer("foo")); err != nil {
			t.Fatal(err)
		}
	}
	sub2.Close()
	for i := 0; i < 5; i++ {
		if err := topic.Publish(native.Pointer("foo")); err != nil {
			t.Fatal(err)
		}
	}
	time.Sleep(time.Second)
	if check := atomic.LoadInt32(&sub1Count); check != 10 {
		t.Fatalf("sub1Count %d should be 10", check)
	}
	if check := atomic.LoadInt32(&sub2Count); check != 5 {
		t.Fatalf("sub2Count %d should be 5", check)
	}
}
