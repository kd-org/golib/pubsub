package pubsub

import (
	"context"
	"errors"

	"gitlab.com/kd-org/golib/native/syncmap"
)

var ErrSubscriberExists = errors.New("subscriber already exists")
var ErrSubscriberNotFound = errors.New("subscriber not found")

type Topic[T any] struct {
	subscribers syncmap.Map[*Subscriber[T], struct{}]
}

func (t *Topic[T]) Subscribe(bufferSize int) (*Subscriber[T], error) {
	sub := &Subscriber[T]{
		topic:    t,
		messages: make(chan *T, bufferSize),
	}
	_, loaded := t.subscribers.LoadOrStore(sub, struct{}{})
	if loaded {
		return nil, ErrSubscriberExists
	}
	return sub, nil
}

func (t *Topic[T]) Unsubscribe(sub *Subscriber[T]) error {
	_, loaded := t.subscribers.LoadAndDelete(sub)
	if !loaded {
		return ErrSubscriberNotFound
	}
	return nil
}

func (t *Topic[T]) Publish(msg *T) {
	t.subscribers.Range(func(sub *Subscriber[T], _ struct{}) bool {
		select {
		case sub.messages <- msg:
		default:
		}
		return true
	})
}

func (t *Topic[T]) PublishContext(ctx context.Context, msg *T) {
	t.subscribers.Range(func(sub *Subscriber[T], _ struct{}) bool {
		select {
		case <-ctx.Done():
			return false
		case sub.messages <- msg:
		}
		return true
	})
}

type Subscriber[T any] struct {
	topic    *Topic[T]
	messages chan *T
}

func (t *Subscriber[T]) Events() <-chan *T {
	return t.messages
}

func (t *Subscriber[T]) Close() error {
	return t.topic.Unsubscribe(t)
}
